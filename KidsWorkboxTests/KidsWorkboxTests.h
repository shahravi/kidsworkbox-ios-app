//
//  KidsWorkboxTests.h
//  KidsWorkbox
//
//  Created by Ravi Shah on 5/5/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#ifndef KidsWorkbox_KidsWorkboxTests_h
#define KidsWorkbox_KidsWorkboxTests_h

#import <XCTest/XCTest.h>
#import <UIKit/UIKit.h>

// Test-subject headers.
#import "AppDelegate.h"
#import "User.h"
#import "LoginViewController.h"

@interface KidsWorkboxTests : XCTestCase {
@private
    AppDelegate    *app_delegate;
    LoginViewController *login_view_controller;
    UIView             *login_view;
}

@end


#endif
