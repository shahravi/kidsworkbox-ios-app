//
//  KidsWorkboxTests.m
//  KidsWorkboxTests
//
//  Created by Ravi Shah on 5/2/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

//#import <UIKit/UIKit.h>
//#import <XCTest/XCTest.h>

#import "KidsWorkboxTests.h"
//@interface KidsWorkboxTests : XCTestCase
//
//@end

@implementation KidsWorkboxTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    app_delegate         = [[UIApplication sharedApplication] delegate];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    login_view_controller = [storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    login_view            = login_view_controller.view;
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    XCTAssert(YES, @"Pass");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

//- (void)testLoginView {
//    //XCTestExpectation *expectation = [self expectationWithDescription:@"Login success block"];
//    
//    [UIApplication sharedApplication].keyWindow.rootViewController = login_view_controller;
//    
//    login_view_controller.username.text = @"shahravi@gmail.com";
//    login_view_controller.password.text = @"kwb12345";
//
//    [login_view_controller authenticateUser:[login_view viewWithTag:3]];
//    
//    //[self waitForExpectationsWithTimeout:10 handler:^(NSError *error) {
//    //    NSLog(@"Visible? - %s", (user_account_nav_controller.visibleViewController == user_account_view_controller) ? "true" : "false");
//    //
//    //    NSLog(@"Timedout for expectation");
//    //}];
//    
//    XCTAssertEqualObjects(login_view_controller.presentedViewController.title, @"User Account Navigation Controller");
//    
//    
//}

- (void) testLoginSuccessBlock {
    //XCTestExpectation *expectation = [self expectationWithDescription:@"Login success block"];
    
    [UIApplication sharedApplication].keyWindow.rootViewController = login_view_controller;
    
    void (^successBlock)(id operation, id responseObject) = [login_view_controller afnetworkingSuccessBlock];
    successBlock(nil, nil);
    
    XCTAssertEqualObjects(login_view_controller.presentedViewController.title, @"User Account Navigation Controller");
}

- (void) testLoginFailureBlock {
    //XCTestExpectation *expectation = [self expectationWithDescription:@"Login success block"];
    
    [UIApplication sharedApplication].keyWindow.rootViewController = login_view_controller;
    
    void (^failureBlock)(id operation, id responseObject) = [login_view_controller afnetworkingFailureBlock];
    failureBlock(nil, nil);
    
    XCTAssertNil(login_view_controller.presentedViewController);
}

@end
