//
//  ArtifactViewController.h
//  KidsWorkbox
//
//  Created by Ravi Shah on 5/30/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ArtifactDelegateProtocol.h"
#import "KidDelegateProtocol.h"

@interface ArtifactViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *artifactImageView;
@property (strong, nonatomic) id <ArtifactDelegateProtocol> delegate;
@property (nonatomic, strong) id <KidDelegateProtocol> kidDelegate;

@property (weak, nonatomic) IBOutlet UILabel *age;

@end
