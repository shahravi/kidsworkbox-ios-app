//
//  UserDelegate.m
//  KidsWorkbox
//
//  Created by Ravi Shah on 5/24/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#import "UserDelegate.h"
#import "UserTableViewControllerBase.h"

@interface UserDelegate() 

@end

@implementation UserDelegate

- (User*) viewControllerUser:(UserTableViewControllerBase*)viewController {
    return _user;
}

- (void) viewController:(UserTableViewControllerBase*)viewController dismissWithUpdatedUser:(User*)user {
    
}

- (void) viewController:(UserTableViewControllerBase*)viewController dismissWithDeletedUser:(User*)user {
    
}

- (void) viewController:(UserTableViewControllerBase*)viewController dismissWithCreatedUser:(User*)user {
    
}

- (void) setUser:(User*)user {
    if (!_user)
        _user = [[User alloc] init];
    _user = user;
}

@end
