//
//  ArtifactDelegateProtocol.h
//  KidsWorkbox
//
//  Created by Ravi Shah on 5/30/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#ifndef KidsWorkbox_ArtifactDelegateProtocol_h
#define KidsWorkbox_ArtifactDelegateProtocol_h
#import "Artifact.h"

@protocol ArtifactDelegateProtocol <NSObject>

@property (strong, nonatomic) Artifact *artifact;

@end


#endif
