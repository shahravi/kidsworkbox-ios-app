//
//  MenuItem.h
//  KidsWorkbox
//
//  Created by Ravi Shah on 5/16/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MenuItem : NSObject
@property NSString *menuItemName;
@property NSString *menuItemImage;
@property NSString *menuItemSegueName;
@end
