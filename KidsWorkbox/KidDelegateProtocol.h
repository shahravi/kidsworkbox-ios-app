//
//  KidDelegateProtocol.h
//  KidsWorkbox
//
//  Created by Ravi Shah on 5/26/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#ifndef KidsWorkbox_KidDelegateProtocol_h
#define KidsWorkbox_KidDelegateProtocol_h
#include <UIKit/UIKit.h>
#import "Kid.h"

@protocol KidDelegateProtocol <NSObject>

@property (strong, nonatomic) Kid* kid;

- (NSMutableArray *)tableViewControllerKidArtifacts:(id)tableVC;

@end

#endif
