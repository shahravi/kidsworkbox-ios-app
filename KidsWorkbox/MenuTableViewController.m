//
//  MenuTableViewController.m
//  KidsWorkbox
//
//  Created by Ravi Shah on 5/16/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#import "MenuTableViewController.h"
#import "MenuItem.h"

@interface MenuTableViewController ()
@property NSMutableArray *menuItems;
@end

@implementation MenuTableViewController

- (void) loadInitialData {
    MenuItem *item1 = [[MenuItem alloc] init];
    item1.menuItemName = @"Home";
    item1.menuItemSegueName = @"HomeSegue";
    [self.menuItems addObject:item1];
    
    MenuItem *item2 = [[MenuItem alloc] init];
    item2.menuItemName = @"How it works?";
    //Change segue name once we have a segue for this page
    item2.menuItemSegueName = @"HomeSegue";
    [self.menuItems addObject:item2];
    
    MenuItem *item3 = [[MenuItem alloc] init];
    item3.menuItemName = @"FAQs";
    //Change segue name once we have a segue for this page
    item3.menuItemSegueName = @"HomeSegue";
    [self.menuItems addObject:item3];
    
    MenuItem *item4 = [[MenuItem alloc] init];
    item4.menuItemName = @"About Us";
    item4.menuItemSegueName = @"AboutUsSegue";
    [self.menuItems addObject:item4];
    
    MenuItem *item5 = [[MenuItem alloc] init];
    item5.menuItemName = @"Contact Us";
    //Change segue name once we have a segue for this page
    item5.menuItemSegueName = @"HomeSegue";
    [self.menuItems addObject:item5];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.menuItems = [[NSMutableArray alloc] init];
    [self loadInitialData];
    
    //self.view.backgroundColor = [UIColor lightGrayColor];
    self.tableView.tableFooterView = [UIView new];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;

    [self.navigationController.navigationBar setTintColor:[UIColor redColor]];
    [self.navigationController.toolbar setTintColor:[UIColor redColor]];
    //Load the home page first time the view is loaded, i.e. not show the user the menu
    [self performSegueWithIdentifier:@"HomeSegue" sender:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [self.menuItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MenuItemPrototypeCell"
                                                            forIndexPath:indexPath];
    
    // Configure the cell...
    MenuItem *menuItem = [self.menuItems objectAtIndex:indexPath.row];
    cell.textLabel.text = menuItem.menuItemName;
//    cell.imageView.image =
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    MenuItem *menuItem = [self.menuItems objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:menuItem.menuItemSegueName sender:self];
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

/*
// Use the following two methods to set footer label for the table
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *footer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 540, 10)];
    footer.backgroundColor = [UIColor lightGrayColor];
    
    UILabel *lbl = [[UILabel alloc]initWithFrame:footer.frame];
    lbl.backgroundColor = [UIColor clearColor];
    lbl.text = @"xWorkbox LLC Copyright 2015";
    lbl.textAlignment = NSTextAlignmentCenter;
    [footer addSubview:lbl];
    
    return footer;
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 10.0;
}
*/
@end
