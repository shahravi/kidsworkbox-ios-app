//
//  Kid.h
//  KidsWorkbox
//
//  Created by Ravi Shah on 5/20/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Kid : NSObject
@property NSInteger kid_id;
@property NSInteger parent_id;
@property NSInteger school_id;
@property NSString *name;
@property NSDate *dob;
@property NSDictionary *picture;
@property NSArray *artifacts;
@end
