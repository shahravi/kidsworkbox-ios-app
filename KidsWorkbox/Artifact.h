//
//  Artifact.h
//  KidsWorkbox
//
//  Created by Ravi Shah on 5/30/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Artifact : NSObject

@property NSDate *created_at;
@property NSDate *updated_at;
@property NSInteger artifact_id;
@property NSInteger kid_id;
@property NSString *name;
@property BOOL parents_favorite;
@property NSDictionary *picture;
@property BOOL tag_art;
@property BOOL tag_english;
@property BOOL tag_math;
@property BOOL tag_other;

//Additional properties that are not in model, but needed for app level processing
@property BOOL edited;

- (Artifact*) initWithDictionary:(NSDictionary*)artifactDictionary;

@end
