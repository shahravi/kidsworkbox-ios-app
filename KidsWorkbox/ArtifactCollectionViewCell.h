//
//  ArtifactCollectionViewCell.h
//  KidsWorkbox
//
//  Created by Ravi Shah on 5/29/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArtifactCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;


@end
