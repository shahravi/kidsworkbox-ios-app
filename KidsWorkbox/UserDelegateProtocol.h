//
//  UserDelegateProtocol.h
//  KidsWorkbox
//
//  Created by Ravi Shah on 5/24/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#ifndef KidsWorkbox_UserDelegateProtocol_h
#define KidsWorkbox_UserDelegateProtocol_h

#import "User.h"
#import "UserTableViewControllerBase.h"

@protocol UserDelegateProtocol <NSObject>

@property (nonatomic, strong) User *user;

- (User*) viewControllerUser:(UserTableViewControllerBase*)viewController;
- (void) viewController:(UserTableViewControllerBase*)viewController dismissWithUpdatedUser:(User*)user;
- (void) viewController:(UserTableViewControllerBase*)viewController dismissWithDeletedUser:(User*)user;
- (void) viewController:(UserTableViewControllerBase*)viewController dismissWithCreatedUser:(User*)user;


@end

#endif
