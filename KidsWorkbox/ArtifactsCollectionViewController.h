//
//  ArtifactsCollectionViewController.h
//  KidsWorkbox
//
//  Created by Ravi Shah on 5/29/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

#import "KidDelegateProtocol.h"

@interface ArtifactsCollectionViewController : UICollectionViewController <UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) id <KidDelegateProtocol> delegate;
- (IBAction)editArtifacts:(UIBarButtonItem *)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *editButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cameraButton;

@end
