//
//  KidsWorkboxEnvironment.h
//  KidsWorkbox
//
//  Created by Ravi Shah on 8/22/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#ifndef KidsWorkbox_KidsWorkboxEnvironment_h
#define KidsWorkbox_KidsWorkboxEnvironment_h

//#define AMAZON_ENV
#define LOCALHOST_ENV

#ifdef AMAZON_ENV
static NSString * const kwbURLString = @"http://www.kidsworkbox.com/";
static NSString * const kwbPicturePrefixString = @"";
#else
//Start rails server like - rails s -b 192.168.1.6 -p 3000
static NSString * const kwbURLString = @"http://192.168.1.6:3000/";
static NSString * const kwbPicturePrefixString = @"http://192.168.1.6:3000/";
#endif

#endif
