//
//  ShowUserTableViewController.h
//  KidsWorkbox
//
//  Created by Ravi Shah on 5/26/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserTableViewControllerBase.h"

@interface ShowUserTableViewController : UserTableViewControllerBase

@end
