//
//  ShowArtifactTableViewController.h
//  KidsWorkbox
//
//  Created by Ravi Shah on 5/22/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KidDelegateProtocol.h"

@interface ShowArtifactsTableViewController : UITableViewController

@property (nonatomic, strong) id <KidDelegateProtocol> delegate;

@end
