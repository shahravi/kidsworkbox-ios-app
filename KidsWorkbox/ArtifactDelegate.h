//
//  ArtifactDelegate.h
//  KidsWorkbox
//
//  Created by Ravi Shah on 5/30/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ArtifactDelegateProtocol.h"

@interface ArtifactDelegate : NSObject <ArtifactDelegateProtocol>

@property (strong, nonatomic) Artifact *artifact;

@end
