//
//  DashboardTableViewController.h
//  KidsWorkbox
//
//  Created by Ravi Shah on 5/17/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserTableViewControllerBase.h"
#import "UserDelegateProtocol.h"

@interface DashboardTableViewController : UserTableViewControllerBase

@property (nonatomic, strong) id <UserDelegateProtocol> userDelegate;

@end
