//
//  AppDelegate.h
//  KidsWorkbox
//
//  Created by Ravi Shah on 5/2/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

