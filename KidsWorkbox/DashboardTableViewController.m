//
//  DashboardTableViewController.m
//  KidsWorkbox
//
//  Created by Ravi Shah on 5/17/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#import "DashboardTableViewController.h"
#import "UserTableViewCell.h"
#import "KidTableViewCell.h"
#import "UIImageView+AFNetworking.h"

#import "ShowUserTableViewController.h"

#import "ArtifactsCollectionViewController.h"

#import "KidDelegate.h"
#import "KidsWorkboxEnvironment.h"

@interface DashboardTableViewController ()

@end

static NSString *UserCellIdentifier = @"UserCellIdentifier";
static NSString *KidCellIdentifier = @"KidCellIdentifier";

@implementation DashboardTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [self.tableView registerClass:[UserTableViewCell class] forCellReuseIdentifier:UserCellIdentifier];
//    [self.tableView registerClass:[KidTableViewCell class] forCellReuseIdentifier:KidCellIdentifier];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    NSInteger count = 0;
    User *currentUser = (User*)[self.userDelegate viewControllerUser:self] ;
    if (currentUser) {
        count++;
        if ([currentUser.kids count] > 0)
            count++;
    }
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    User *currentUser = (User*)[self.userDelegate viewControllerUser:self] ;
    switch (section) {
    case 0:
            return 1;
    case 1:
            return [currentUser.kids count];
    }
    
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"Row# : %lu", (long)indexPath.row);
    User *currentUser = (User*)[self.userDelegate viewControllerUser:self] ;
    // Configure the cell...
    switch (indexPath.section) {
        case 0: {
            UserTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:UserCellIdentifier forIndexPath:indexPath];

            cell.userNameLabel.text = currentUser.name;
            cell.numberOfKidsLabel.text = [NSString stringWithFormat:@"%lu Kids", (unsigned long)[currentUser.kids count] ];
            
            NSLog(@"Picture %@", currentUser.picture);
            NSLog(@"Picture %@", currentUser.picture[@"url"]);
            NSLog(@"Picture %@", currentUser.picture[@"thumb"][@"url"]);
            
            NSString *urlString = NULL;
            if ([currentUser.picture[@"thumb"][@"url"] isEqualToString:@"/default-artifact.gif"])
                urlString = [kwbURLString stringByAppendingString:currentUser.picture[@"thumb"][@"url"]];
            else
                urlString = [kwbPicturePrefixString stringByAppendingString:currentUser.picture[@"thumb"][@"url"]];

            
            NSURL *url = [NSURL URLWithString:urlString];
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
//            UIImage *placeholderImage = [UIImage imageNamed:@"placeholder"];
            
            __weak UserTableViewCell *weakCell = cell;
            
            [cell.userThumbnailImage setImageWithURLRequest:request
                                  placeholderImage:nil
                                           success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                               weakCell.userThumbnailImage.image = image;
                                               [weakCell setNeedsLayout];
                                           } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                               
                                           }];
            
            return cell;
        }
        case 1: {
            KidTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:KidCellIdentifier forIndexPath:indexPath];
            NSDictionary *kidDictionary = [currentUser.kids objectAtIndex:(indexPath.row)];
            Kid *kid = [[Kid alloc]init];
            [kid setValuesForKeysWithDictionary:kidDictionary];
            
            cell.textLabel.text = kid.name;
            NSString *urlString = NULL;
            
            if ([kid.picture[@"thumb"][@"url"] isEqualToString:@"/default-artifact.gif"])
                urlString = [kwbURLString stringByAppendingString:kid.picture[@"thumb"][@"url"]];
            else
                urlString = [kwbPicturePrefixString stringByAppendingString:kid.picture[@"thumb"][@"url"]];
            
            NSURL *url = [NSURL URLWithString:urlString];
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            //            UIImage *placeholderImage = [UIImage imageNamed:@"placeholder"];
            
            __weak KidTableViewCell *weakCell = cell;
            
            [cell.imageView setImageWithURLRequest:request
                                           placeholderImage:nil
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                                        weakCell.imageView.image = image;
                                                        [weakCell setNeedsLayout];
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                                        
                                                    }];
            
            return cell;
        }
            
        default: {
            UITableViewCell *cell = nil;
            return cell;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
            return 150.0;
        default:
            return 100.0;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 1:
            return @"Kids";
            
        default:
            return nil;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return 0.001;
            
        default:
            return 44.0;
    }
}

//Since the accessory on kid is Disclosure, this method is called
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%lu", (long)indexPath.section);
    
    switch (indexPath.section) {
        case 1:
            return;
        default:
            return;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    User *currentUser = (User*)[self.userDelegate viewControllerUser:self] ;
    
    if ([[segue identifier] isEqualToString:@"ShowArtifactsSegue"]) {
        if ([sender isKindOfClass:[KidTableViewCell class]]) {
            KidTableViewCell *cell = sender;
            NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
            
//            ShowArtifactsTableViewController *showArtifactsTVC = [segue destinationViewController];
//            showArtifactsTVC.delegate = (KidDelegate*)[[KidDelegate alloc] init];
            
            ArtifactsCollectionViewController *artifactsCVC = [segue destinationViewController];
            artifactsCVC.delegate = (KidDelegate*)[[KidDelegate alloc] init];
            
            NSDictionary *kidDictionary = [currentUser.kids objectAtIndex:(indexPath.row)];
            Kid *kid = [[Kid alloc] init];
            [kid setValuesForKeysWithDictionary:kidDictionary];
            
            artifactsCVC.delegate.kid = kid;
        }
        
    }
    
    if ([[segue identifier] isEqualToString:@"ShowUserSegue"]) {
//        if ([sender isKindOfClass:[UserTableViewCell class]]) {
//            UserTableViewCell *cell = sender;
//            NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
//            ShowUserTableViewController *showVC = [segue destinationViewController];
//            
//        }
    }
    
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be .
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
