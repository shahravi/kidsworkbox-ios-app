//
//  UserTableViewCell.h
//  KidsWorkbox
//
//  Created by Ravi Shah on 5/17/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberOfKidsLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userThumbnailImage;
@property (weak, nonatomic) IBOutlet UIButton *subscribeAChildButton;

@end
