//
//  ArtifactViewController.m
//  KidsWorkbox
//
//  Created by Ravi Shah on 5/30/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#import "ArtifactViewController.h"
#import "UIImageView+AFNetworking.h"

#import "KidsWorkboxEnvironment.h"


@interface ArtifactViewController ()
- (void) displayArtifact;
@end

@implementation ArtifactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //set up the toolbar
    [self.navigationController setToolbarHidden:NO];
    [self.navigationController.toolbar setBarStyle:UIBarStyleDefault];

    //Enable interaction with the image
    self.artifactImageView.userInteractionEnabled = TRUE;
    
    //Add tap gesture to image for showing / hiding toolbar
    UITapGestureRecognizer * singleTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(tappedImage:)];
    [self.artifactImageView addGestureRecognizer:singleTap];
    
    //Add swipe gesture to image for showing previous / next image
    UISwipeGestureRecognizer *rightSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipedImage:)];
    UISwipeGestureRecognizer *leftSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipedImage:)];
    
    // Setting the swipe direction.
    [leftSwipe setDirection:UISwipeGestureRecognizerDirectionLeft];
    [rightSwipe setDirection:UISwipeGestureRecognizerDirectionRight];

    [self.artifactImageView addGestureRecognizer:(rightSwipe)];
    [self.artifactImageView addGestureRecognizer:(leftSwipe)];
    
    [self displayArtifact];
}

- (void) viewWillDisappear:(BOOL)animated {
    //reset up the toolbar
    [self.navigationController setToolbarHidden:YES];
}

- (IBAction)swipedImage:(UISwipeGestureRecognizer *)swipe {
    NSLog(@"Swiped Image");
    if (swipe.direction == UISwipeGestureRecognizerDirectionLeft) {
        NSLog(@"Left Swipe");
    }
    
    if (swipe.direction == UISwipeGestureRecognizerDirectionRight) {
        NSLog(@"Right Swipe");
    }
}

- (IBAction)tappedImage:(id)sender {
    NSLog(@"Tapped Image");
    [self.navigationController hidesBarsOnTap];
    if ([[self.navigationController navigationBar] isHidden]) {
        [self.navigationController setNavigationBarHidden:NO animated:YES];
        [self.navigationController setToolbarHidden:NO animated:YES];
    }
    else {
        [self.navigationController setNavigationBarHidden:YES animated:YES];
        [self.navigationController setToolbarHidden:YES animated:YES];
    }
}

- (IBAction)turnImage:(id)sender {
    NSLog(@"Turn Image");
//    self.artifactImageView.center = CGPointMake(100.0, 100.0);
    self.artifactImageView.transform = CGAffineTransformMakeRotation(M_PI_2);
}

- (IBAction)favoriteImage:(id)sender {
    NSLog(@"Favorite Image");
}

- (IBAction)tagImage:(id)sender {
    NSLog(@"Tag Image");
}

- (IBAction)shareImage:(id)sender {
    NSLog(@"Share Image");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) displayArtifact {
    Artifact *artifact = [self.delegate artifact];
    NSString *urlString = [kwbPicturePrefixString stringByAppendingString:artifact.picture[@"url"]];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    [self.artifactImageView setImageWithURLRequest:request
                          placeholderImage:nil
                                   success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                       self.artifactImageView.image = image;
                                   } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                       
                                   }];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents *components = [calendar components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear
                                               fromDate:artifact.created_at];

    NSString *monthName = [[[[NSDateFormatter alloc] init] standaloneMonthSymbols] objectAtIndex:(components.month - 1)];
    
    self.title = [NSString stringWithFormat:@"%@ %@", monthName, [@(components.year) stringValue]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
