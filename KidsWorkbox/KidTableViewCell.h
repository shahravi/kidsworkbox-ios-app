//
//  KidTableViewCell.h
//  KidsWorkbox
//
//  Created by Ravi Shah on 5/19/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KidTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *kidNameLabel;

@end
