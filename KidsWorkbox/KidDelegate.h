//
//  KidDataSourceDelegate.h
//  KidsWorkbox
//
//  Created by Ravi Shah on 5/26/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Kid.h"
#import "KidDelegateProtocol.h"

@interface KidDelegate : NSObject <KidDelegateProtocol>

@property (strong, nonatomic) Kid* kid;

@end
