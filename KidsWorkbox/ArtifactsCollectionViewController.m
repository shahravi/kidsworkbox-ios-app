//
//  ArtifactsCollectionViewController.m
//  KidsWorkbox
//
//  Created by Ravi Shah on 5/29/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#import "ArtifactsCollectionHeaderView.h"
#import "ArtifactsCollectionViewController.h"
#import "ArtifactCollectionViewCell.h"
#import "ArtifactViewController.h"
#import "ArtifactDelegate.h"
#import "Artifact.h"

#import "KidDelegate.h"

#import "CaptureArtifactViewController.h"
#import "UIImageView+AFNetworking.h"

#import "KidsWorkboxEnvironment.h"

@interface ArtifactsCollectionViewController ()

@end

@implementation ArtifactsCollectionViewController

static NSString * const reuseIdentifier = @"ArtifactCell";
NSMutableArray *artifactsByMonth = NULL;
bool editMode = FALSE;

- (void)viewDidLoad {
    [super viewDidLoad];

    Kid *currentKid = [self.delegate kid];
    NSLog(@"Kid: %@", currentKid.name);

    self.title = [[currentKid.name componentsSeparatedByString:@" "] objectAtIndex:0];
    
    //set up the toolbar
    [self.navigationController setToolbarHidden:NO];
    [self.navigationController.toolbar setBarStyle:UIBarStyleDefault];

    NSLog(@"Artifacts: %@", currentKid.artifacts);
    artifactsByMonth = nil;
    [self artifactsSortedByAge:[currentKid artifacts]];
    
    // Uncomment the following line to preserve selection between presentations
    self.clearsSelectionOnViewWillAppear = NO;
    
    self.collectionView.allowsMultipleSelection = YES;
  
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setToolbarHidden:NO];
    [self.navigationController.toolbar setBarStyle:UIBarStyleDefault];
    
    editMode = FALSE;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    NSInteger numberofSections = [artifactsByMonth count];
    return numberofSections;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    NSInteger numberOfItemsInSection = [[artifactsByMonth objectAtIndex:section] count];
    return numberOfItemsInSection;
    
    //return [[[self.delegate kid] artifacts] count];
}

- (ArtifactCollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    //NSInteger count = [[[self.delegate kid] artifacts] count];
    
    // Configure the cell
    NSLog(@"Configuring cell %ld %ld", (long)indexPath.row, (long)indexPath.section);
    ArtifactCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    //Artifact *artifact = [[Artifact alloc] initWithDictionary:[[[self.delegate kid] artifacts] objectAtIndex:(count - (indexPath.row + 1))]]; //count - (indexPath.row + 1) reverses the order

    Artifact *artifact = [[Artifact alloc] init];
    artifact = [[artifactsByMonth objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
    NSString *urlString = [kwbPicturePrefixString stringByAppendingString:artifact.picture[@"thumb"][@"url"]];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    //            UIImage *placeholderImage = [UIImage imageNamed:@"placeholder"];
    
    __weak ArtifactCollectionViewCell *weakCell = cell;
    
    [cell.imageView setImageWithURLRequest:request
                          placeholderImage:nil
                                   success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                       weakCell.imageView.image = image;
                                       [weakCell setNeedsLayout];
                                   } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                       
                                   }];
    
    
    return cell;
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if (editMode)
        return NO;
    else
        return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:(ArtifactCollectionViewCell *)sender];
    if ([sender isKindOfClass:[ArtifactCollectionViewCell class]]) {
        NSInteger count = [[[self.delegate kid] artifacts] count];
        
        ArtifactViewController *artifactVC = [segue destinationViewController];
        artifactVC.delegate = (ArtifactDelegate*)[[ArtifactDelegate alloc] init];
        artifactVC.delegate.artifact = [[artifactsByMonth objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        
        //[[Artifact alloc] initWithDictionary:[[[self.delegate kid] artifacts] objectAtIndex:(count - (indexPath.row + 1))]]; //Matches the reverse order while listing artifacts
        
        artifactVC.kidDelegate = (KidDelegate *)[[KidDelegate alloc] init];
        artifactVC.kidDelegate.kid = [self.delegate kid];
    } else {
        if ([segue.identifier isEqualToString:@"CaptureArtifactSegue"]) {
            CaptureArtifactViewController *captureVC = [segue destinationViewController];
            captureVC.kidDelegate = (KidDelegate*)[[KidDelegate alloc] init];
            captureVC.kidDelegate.kid = [self.delegate kid];
        }
    }
    
    [self.collectionView deselectItemAtIndexPath:indexPath animated:YES];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger numberOfImagesPerRow = 4;
    CGFloat width = ([[UIScreen mainScreen] bounds].size.width - numberOfImagesPerRow) / (numberOfImagesPerRow + 1);
    return CGSizeMake(width, width);
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    UICollectionReusableView *reusableView = nil;
    if (kind == UICollectionElementKindSectionHeader) {
        ArtifactsCollectionHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"ArtifactsCollectionHeader" forIndexPath:indexPath];
        Kid *currentKid = [self.delegate kid];
        Artifact *artifact = [[Artifact alloc] init];
        artifact = [[artifactsByMonth objectAtIndex:indexPath.section] objectAtIndex:0];

        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *components = [calendar components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear
                                                   fromDate:currentKid.dob];
        [components setDay:1]; //calculate age from the first day of kid's birth month
        
        components = [calendar components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear
                                                   fromDate:[calendar dateFromComponents:components]
                                                     toDate:artifact.created_at
                                                    options:0];

        headerView.title.text = [NSString stringWithFormat:@"%@ Years %@ Months",[@(components.year) stringValue], [@(components.month) stringValue]];
        
        reusableView = headerView;
    }
    return reusableView;
}

#pragma mark <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"Selected cell %ld %ld", (long)indexPath.row, (long)indexPath.section);
    Artifact *artifact = [[Artifact alloc] init];
    artifact = [[artifactsByMonth objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    artifact.edited = TRUE;
    
    [[artifactsByMonth objectAtIndex:indexPath.section] replaceObjectAtIndex:indexPath.row withObject:artifact];
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    Artifact *artifact = [[Artifact alloc] init];
    artifact = [[artifactsByMonth objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    artifact.edited = FALSE;
    
    [[artifactsByMonth objectAtIndex:indexPath.section] replaceObjectAtIndex:indexPath.row withObject:artifact];
}

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

- (IBAction)editArtifacts:(UIBarButtonItem *)sender {
    if (editMode) {
        editMode = FALSE;
        [sender setTitle:@"Edit"];
    } else {
        editMode = TRUE;
        [sender setTitle:@"Cancel"];
    }
}

- (NSInteger) monthOfDate:(NSDate*)date {
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:NSCalendarUnitMonth fromDate:date];
    
    return [components month];
}

- (NSInteger) yearOfDate:(NSDate*)date {
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:NSCalendarUnitMonth fromDate:date];
    
    return [components year];
}

- (void)artifactsSortedByAge:(NSArray*)artifactsArray {
    if (artifactsByMonth) {
        return;
    }
    
    NSInteger artifactsCount = [artifactsArray count];
    
    for (NSInteger i = 0; i < artifactsCount; i++) {
        Artifact *artifact = [[Artifact alloc] initWithDictionary:[artifactsArray objectAtIndex:i]];
        NSInteger j = [artifactsByMonth count];
        
        //Go through all the arrays of month, and see if there is already an array that stores objects of the month in which this artifact was created
        if (j > 0) {
            for (j=[artifactsByMonth count]; j > 0; j--) {
                Artifact *firstObjectOfMonth = [[artifactsByMonth objectAtIndex:(j - 1)] objectAtIndex:0];
                if (([self monthOfDate:firstObjectOfMonth.created_at] == [self monthOfDate:artifact.created_at])
                    && ([self yearOfDate:firstObjectOfMonth.created_at] == [self yearOfDate:artifact.created_at])) {
                    //There is already an array for this month, so add to it
                    [[artifactsByMonth objectAtIndex:(j - 1)] addObject:artifact];
                    break;
                }
            }
            
            //There is no array for this month, so add at the end
            if (j == 0) {
                NSMutableArray *monthArray = [[NSMutableArray alloc] init];
                [monthArray addObject:artifact];
                [artifactsByMonth addObject:monthArray];
            }
        } else {
            //There are no arrays yet. Add at the beginning
            NSMutableArray *monthArray = [[NSMutableArray alloc] init];
            [monthArray addObject:artifact];
            
            artifactsByMonth = [[NSMutableArray alloc] init];
            [artifactsByMonth addObject:monthArray];
            //[[artifactsByMonth objectAtIndex:[artifactsByMonth count]] addObject:@(i)];
        }
    }
}

@end
