//
//  Kid.m
//  KidsWorkbox
//
//  Created by Ravi Shah on 5/20/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#import "Kid.h"

@implementation Kid

//Overridng this method to avoid getting error for undefined keys
- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    if ([key isEqualToString:@"id"])
        [self setValue:value forKey:@"kid_id"];
    
    return;
}

- (void)setValue:(id)value forKey:(NSString *)key {
    if ([key isEqualToString:@"dob"]) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];

        NSDate *date = [dateFormatter dateFromString:value];
        [super setValue:date forKey:key];
    } else {
        [super setValue:value forKey:key];
    }

    return;
}

@end
