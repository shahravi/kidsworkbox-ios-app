//
//  CaptureArtifactViewController.m
//  KidsWorkbox
//
//  Created by Ravi Shah on 8/22/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#import "CaptureArtifactViewController.h"
#import "AFNetworking.h"
#import "KidsWorkboxEnvironment.h"


@interface CaptureArtifactViewController ()

@end

@implementation CaptureArtifactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //On simulator or older iPods, there may be no camera. So handle that gracefully
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)takePhoto:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
    picker.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;

    [self presentViewController:picker animated:YES completion:NULL];
}


- (IBAction)selectPhoto:(UIButton *)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
    
    [self presentViewController:picker animated:YES completion:NULL];
}

- (IBAction)cancelPhoto:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSLog(@"%@", info);
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
    NSData *mediaData = [[NSData alloc] init];
    
    if (chosenImage) {
        mediaData = UIImageJPEGRepresentation(chosenImage, 0.5);
    } else {
        if (videoURL) {
            mediaData = [NSData dataWithContentsOfURL:videoURL];
        }
    }
    //NSData *videoData = [NSData dataWithContentsOfURL:videoURL];
    
//    NSData *videoData = [NSData dataWithContentsOfURL:[NSURL fileURLWithPath:videoPath]];
//    [formData appendPartWithFileData:videoData name:@"video" fileName:@"video.mov" mimeType:@"video/quicktime"];

    //self.imageView.image = chosenImage;
    //NSData *imageData = UIImageJPEGRepresentation(chosenImage, 0.5);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSString *kidIdString = [@([[self.kidDelegate kid] kid_id]) stringValue];
    NSDictionary *params  = @{
                              @"kid_id" : kidIdString
                              };
    //http://stackoverflow.com/questions/19836432/uploading-image-with-afnetworking-2-0
    NSString *artifactUploadURL = [kwbURLString stringByAppendingString:[NSString stringWithFormat:@"/kids/%@/artifacts",kidIdString]];
    [manager POST:artifactUploadURL
       parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
           //do not put image inside parameters dictionary as I did, but append it!
           if (chosenImage) {
               [formData appendPartWithFileData:mediaData name:@"kid[artifacts][0][picture]" fileName:@"photo.jpg" mimeType:@"image/jpeg"];
           } else {
               if (videoURL) {
                   [formData appendPartWithFileData:mediaData name:@"kid[artifacts][0][picture]" fileName:@"video.mov" mimeType:@"video/quicktime"];
               }
           }
           //[formData appendPartWithFileData:mediaData name:@"kid[artifacts][0][picture]" fileName:@"photo.jpg" mimeType:@"image/jpeg"];
       } success:^(AFHTTPRequestOperation *operation, id responseObject) {
           NSLog(@"Success: %@ ***** %@", operation.responseString, responseObject);
       } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
           NSLog(@"Error: %@ ***** %@", operation.responseString, error);
       }];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    [self dismissViewControllerAnimated:NO completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
    [self dismissViewControllerAnimated:NO completion:NULL];
}
@end
