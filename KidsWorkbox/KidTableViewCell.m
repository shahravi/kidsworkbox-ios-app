//
//  KidTableViewCell.m
//  KidsWorkbox
//
//  Created by Ravi Shah on 5/19/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#import "KidTableViewCell.h"

@implementation KidTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
