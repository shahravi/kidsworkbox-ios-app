//
//  CaptureArtifactViewController.h
//  KidsWorkbox
//
//  Created by Ravi Shah on 8/22/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KidDelegateProtocol.h"

@interface CaptureArtifactViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
//@property (weak, nonatomic) IBOutlet UIImageView *imageView;
- (IBAction)takePhoto:(UIButton *)sender;
- (IBAction)selectPhoto:(id)sender;
- (IBAction)cancelPhoto:(UIButton *)sender;
@property (strong, nonatomic) id <KidDelegateProtocol> kidDelegate;

@end
