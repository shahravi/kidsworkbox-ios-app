//
//  UserTableViewCell.m
//  KidsWorkbox
//
//  Created by Ravi Shah on 5/17/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#import "UserTableViewCell.h"

@implementation UserTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
//    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
//    
//    if (self) {
//        self.userNameLabel = [[UILabel alloc] init];
//        [self.contentView addSubview:self.userNameLabel];
//    }
//}



@end
