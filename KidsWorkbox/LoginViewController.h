//
//  KidsWorkboxLoginViewController.h
//  KidsWorkbox
//
//  Created by Ravi Shah on 5/2/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserDelegate.h"

@interface LoginViewController : UIViewController

@property (nonatomic)  User *currentUser;

@property (weak, nonatomic) IBOutlet UITextField *username;
@property (weak, nonatomic) IBOutlet UITextField *password;

- (id) afnetworkingSuccessBlock;
- (id) afnetworkingFailureBlock;
- (IBAction)authenticateUser:(id)sender;

@property (strong, nonatomic) UserDelegate *userDelegate;

@end
