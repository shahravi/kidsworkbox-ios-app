//
//  ArtifactCollectionViewCell.m
//  KidsWorkbox
//
//  Created by Ravi Shah on 5/29/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#import "ArtifactCollectionViewCell.h"
#import "CustomCellBackground.h"

@implementation ArtifactCollectionViewCell

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        // change to our custom selected background view
        CustomCellBackground *backgroundView = [[CustomCellBackground alloc] initWithFrame:CGRectZero];
        backgroundView.layer.borderWidth = 3.0f;
        backgroundView.layer.borderColor = [UIColor blueColor].CGColor;

        self.selectedBackgroundView = backgroundView;
    }
    return self;
}

@end
