//
//  UserDelegate.h
//  KidsWorkbox
//
//  Created by Ravi Shah on 5/24/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "UserDelegateProtocol.h"

@interface UserDelegate : NSObject  <UserDelegateProtocol>

@property (strong, nonatomic) User* user;

@end
