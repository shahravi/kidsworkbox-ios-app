//
//  KidsWorkboxLoginViewController.m
//  KidsWorkbox
//
//  Created by Ravi Shah on 5/2/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#import "LoginViewController.h"
#import "DashboardTableViewController.h"

#import "UserDelegate.h"

#import "AFNetworking.h"
#import "KidsWorkboxEnvironment.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)unwindToLogin:(UIStoryboardSegue *)segue {
    
}

- (id) afnetworkingSuccessBlock {
    id block = ^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"operation: %@", operation);
//        NSLog(@"JSON: %@", responseObject);
        
        NSDictionary *userDictionary = responseObject;
        self.currentUser = [[User alloc] initWithDictionary:userDictionary];
    
        [self performSegueWithIdentifier:@"LoginSegue" sender:self];
    };
    
    return block;
}

- (id) afnetworkingFailureBlock {
    id block = ^(AFHTTPRequestOperation *operation, NSError *error) {
        if (error && operation) {
            NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
            NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData:errorData
                                                                           options:kNilOptions
                                                                             error:nil];
            NSLog(@"Error data: %@", serializedData);
            NSLog(@"Error message: %@", serializedData[@"message"]);
        }
        
    };
    
    return block;
}

- (IBAction)authenticateUser:(id)sender {
    //TODO - remove the override after development
    self.username.text = @"shahravi@gmail.com";
    self.password.text = @"kwb12345";
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *params  = @{
                              @"session[email]" : self.username.text, //_currentUser.email,
                              @"session[password]" : self.password.text //_currentUser.password
                              };
    
// URL : http://www.kidsworkbox.com/sessions.json
   
    NSString *loginURL = [kwbURLString stringByAppendingString:@"sessions.json"];
    [manager POST:loginURL
       parameters:params
          success:self.afnetworkingSuccessBlock
          failure:self.afnetworkingFailureBlock];
    
    NSLog(@"self: %@", self);
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"LoginSegue"]) {
        DashboardTableViewController *dashboardTVC = [segue destinationViewController];
        
        dashboardTVC.userDelegate = (UserDelegate*)[[UserDelegate alloc] init];
        dashboardTVC.userDelegate.user = self.currentUser;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//Code written while learning bjective-c
//            NSError *jsonParsingError = nil;
//            NSString *userDataString = responseDict[@"user"];
//            NSData *userData = [userDataString dataUsingEncoding:NSUTF8StringEncoding];
//
//            NSDictionary *user = [NSJSONSerialization JSONObjectWithData:userData
//                                                           options:kNilOptions
//                                                              error:&jsonParsingError];

//            NSError *jsonParsingError = nil;
//            NSString *userDataString = responseDict[@"user"];
//            NSData *userData = responseDict;

//            NSDictionary *user = [NSJSONSerialization JSONObjectWithData:userData
//                                                                options:kNilOptions
//                                                                          error:&jsonParsingError];


