//
//  User.h
//  KidsWorkbox
//
//  Created by Ravi Shah on 5/2/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property NSInteger user_id;
@property NSString *name;
@property NSString *password;
@property NSString *email;
@property Boolean admin;
@property Boolean activated;
@property NSDictionary *picture;
@property NSInteger identity_id;
@property NSString *identity_type;
@property NSArray *kids;
@property NSArray *collections;

- (User*) initWithDictionary:(NSDictionary*)userDictionary;
@end
