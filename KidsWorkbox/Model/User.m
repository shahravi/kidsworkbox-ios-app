//
//  User.m
//  KidsWorkbox
//
//  Created by Ravi Shah on 5/2/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#import "User.h"

@interface User()

@end

@implementation User

//Overridng this method to avoid getting error for undefined keys
- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    if ([key isEqualToString:@"id"])
        [self setValue:value forKey:@"user_id"];
    
    return;
}

- (void) setValue:(id)value forKey:(NSString *)key {
    if([key isEqualToString:@"identity_id"] && (value == nil)) {
        //don't set nil value for integer 
        return;
    }

    [super setValue:value forKey:key];
}

- (User*) initWithDictionary:(NSDictionary*)userDictionary {
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:userDictionary];
        return self;
    } else {
        return nil;
    }
}

@end
