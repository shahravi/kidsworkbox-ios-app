//
//  UserTableViewControllerBase.h
//  KidsWorkbox
//
//  Created by Ravi Shah on 5/24/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserTableViewControllerBase : UITableViewController

@end
