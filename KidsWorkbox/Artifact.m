//
//  Artifact.m
//  KidsWorkbox
//
//  Created by Ravi Shah on 5/30/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#import "Artifact.h"

@implementation Artifact

//Overridng this method to avoid getting error for undefined keys
- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    if ([key isEqualToString:@"id"])
        [self setValue:value forKey:@"artifact_id"];
    
    return;
}

-(void)setValue:(id)value forKey:(NSString *)key {
    if ([key isEqualToString:@"created_at"] || [key isEqualToString:@"updated_at"]) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
        
        NSDate *date = [dateFormatter dateFromString:value];
        [super setValue:date forKey:key];
    } else {
        [super setValue:value forKey:key];
    }
    
    return;

    
    //_created_at	__NSCFString *	@"2015-08-25T22:32:30.000Z"
}

- (Artifact*) initWithDictionary:(NSDictionary*)artifactDictionary {
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:artifactDictionary];
        return self;
    } else {
        return nil;
    }
}

@end
