//
//  ArtifactsCollectionHeaderView.h
//  KidsWorkbox
//
//  Created by Ravi Shah on 8/29/15.
//  Copyright (c) 2015 xWorkbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArtifactsCollectionHeaderView : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UILabel *title;

@end
